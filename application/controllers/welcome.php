
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Welcome extends MY_Controller {

	function __construct(){
		parent::__construct();
		$this->load->model('bug');
	} 


	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		parent::index('graficos/index');		
	}

	public function grafico_aberto_severidade($produto = 1){
		$this->conteudo['abertos_por_severidade'] = $this->_graficoAbertosPorSeveridade($produto);
		parent::index('graficos/severidade_aberto');		
	}

	public function grafico_aberto_tipo($produto = 1){
		$this->conteudo['abertos_por_tipo'] = $this->_graficoAbertosPorTipo($produto);
		parent::index('graficos/tipo_aberto');		
	}

	public function grafico_encontrado_tipo($produto = 1){
		$this->conteudo['encontrados_por_tipo'] = $this->_graficoEncontradosPorTipo($produto);
		parent::index('graficos/tipo_encontrado');		
	}

	public function grafico_defeitos_todos_produtos_encontrado_tipo(){
		$this->conteudo['encontrados_por_tipo'] = $this->_graficoDefeitosTodosProdutosEncontradosPorTipo();
		parent::index('graficos/tipo_encontrado_todos_produtos');		
	}	

	public function grafico_encontrados_corrigidos($produto = 1){
		$retorno = $this->_graficoEncontradosVSCorrigidos($produto);
		$this->conteudo['encontrados'] = $retorno['encontrados'];
		$this->conteudo['corrigidos'] = $retorno['corrigidos'];
		parent::index('graficos/encontrados_corrigidos');		
	}

	public function grafico_encontrados_corrigidos_critical($produto = 1){
		$retorno = $this->_graficoEncontradosVSCorrigidosCritical($produto);
		$this->conteudo['encontrados'] = $retorno['encontrados'];
		$this->conteudo['corrigidos'] = $retorno['corrigidos'];
		parent::index('graficos/encontrados_corrigidos_graves');		
	}

	public function grafico_testes_executados($produto = 1){
		$retorno = $this->_graficoTestesExecutados($produto);
		$this->conteudo['testes'] = $retorno;
		parent::index('graficos/testes_executados_ultima_build');
	}

	public function grafico_testes_aprovados($produto = 1){
		$retorno = $this->_graficoTestesAprovados($produto);
		$this->conteudo['testes'] = $retorno;
		parent::index('graficos/testes_aprovados_ultima_build');
	}

	public function grafico_testes_aprovados_categoria($categoria = 1){
		$retorno = $this->_graficoTestesAprovadosCategoria(1, $categoria);

		$array['Executados e aprovados'] = $retorno['Executados e aprovados']; 
		$array['Não Executados ou reprovados'] = $retorno['Não Executados ou reprovados'];

		$this->conteudo['testes'] = $array;
		$this->conteudo['categoria'] = $retorno['Categoria'];
		$this->conteudo['listacategorias'] = $retorno['listacategorias'];
		parent::index('graficos/testes_aprovados_categoria');

	}


	public function grafico_reabertos($produto = 1){
		$retorno = $this->_graficoReabertos($produto);
		$this->conteudo['reabertos'] = $retorno['reabertos'];
		parent::index('graficos/reabertos');		
	}


	public function _graficoAbertosPorSeveridade($produto) {
		$array_final = array();
		$abertos_por_severidade = $this->bug->defeitosAbertosPorSeveridade($produto);
		
  		foreach ($abertos_por_severidade as $item) {
  			if( array_key_exists($item->bug_severity, $array_final) ){
  				$array_final[$item->bug_severity]++;

  			} else {
  				$array_final[$item->bug_severity] = 1;  				
  			}
  		}			

  		ksort($array_final);
  		return $array_final;  		
	}

	public function _graficoAbertosPorTipo($produto) {
		$array_final = array();
		$abertos_por_tipo = $this->bug->defeitosAbertosPorTipo($produto);
		
  		foreach ($abertos_por_tipo as $item) {
  			if( array_key_exists($item->cf_bugtype, $array_final) ){
  				$array_final[$item->cf_bugtype]++;

  			} else {
  				$array_final[$item->cf_bugtype] = 1;  				
  			}
  		}			

  		ksort($array_final);
  		return $array_final;  		
	}

	public function _graficoEncontradosPorTipo($produto) {

		$array_final = array();
		$abertos_por_tipo = $this->bug->defeitosEncontradosPorTipo($produto);
		
  		foreach ($abertos_por_tipo as $item) {
  			if( array_key_exists($item->cf_bugtype, $array_final) ){
  				$array_final[$item->cf_bugtype]++;

  			} else {
  				$array_final[$item->cf_bugtype] = 1;  				
  			}
  		}			

  		ksort($array_final);
  		return $array_final;  		
	}

	public function _graficoDefeitosTodosProdutosEncontradosPorTipo() {

		$array_final = array();
		$abertos_por_tipo = $this->bug->defeitosTodosProdutosEncontradosPorTipo();
		
  		foreach ($abertos_por_tipo as $item) {
  			if( array_key_exists($item->cf_bugtype, $array_final) ){
  				$array_final[$item->cf_bugtype]++;

  			} else {
  				$array_final[$item->cf_bugtype] = 1;  				
  			}
  		}			

  		ksort($array_final);
  		return $array_final;  		
	}

	public function _graficoEncontradosVSCorrigidos($produto = 1){
		$todos_bugs = $this->bug->defeitosOrdenados($produto, "ASC");
		$todos_bugs_modificados = $this->bug->defeitosFechadosOrdenados($produto, "DESC");
		//die(var_dump($todos_bugs));

		if(count($todos_bugs) == 0){
			$retorno['encontrados'] = 0;
			$retorno['corrigidos'] = 0;
			return $retorno;
		}

		$data_inicio = date("Y-m-d", strtotime($todos_bugs[0]->creation_ts));
		$data_fim = date("Y-m-d");

		if($data_fim < date("Y-m-d", strtotime($todos_bugs[count($todos_bugs)-1]->delta_ts)))
			$data_fim = date("Y-m-d", strtotime($todos_bugs[count($todos_bugs)-1]->delta_ts));

		//Como eh o primeiro e foi ordena do por ele, posso pegar com esse numero magico
		$data_atual = date("Y-m-d", strtotime($todos_bugs[0]->creation_ts));	
		$encontrados = array();
		$soma_encontrados = 0;
		while($data_atual <= $data_fim ){
			foreach ($todos_bugs as $item) {
				if( $data_atual == date("Y-m-d", strtotime($item->creation_ts) ) ){
					$soma_encontrados ++;
				}
			}
			$encontrados[$data_atual] = $soma_encontrados;
			$data_atual = date("Y-m-d", strtotime( date("Y-m-d", strtotime($data_atual) ). " + 1 day"));
		}
		$data_atual = date("Y-m-d", strtotime($todos_bugs[0]->creation_ts));	
		$corrigidos = array();
		$soma_corrigidos = 0;
		while($data_atual <= $data_fim ){
			foreach ($todos_bugs as $item) {
				if( $data_atual == date("Y-m-d", strtotime($item->delta_ts) ) AND ($item->is_open == 0) ){
					$soma_corrigidos ++;
				}
			}
			$corrigidos[$data_atual] = $soma_corrigidos;
			$data_atual = date("Y-m-d", strtotime( date("Y-m-d", strtotime($data_atual) ). " + 1 day"));
		}
		//die(var_dump($data_fim));
		$retorno['encontrados'] = $encontrados;
		$retorno['corrigidos'] = $corrigidos;
		return $retorno;
	}


	public function _graficoEncontradosVSCorrigidosCritical($produto = 1){
		$todos_bugs = $this->bug->defeitosCriticalOrdenados($produto, "ASC");
		$todos_bugs_modificados = $this->bug->defeitosCriticalFechadosOrdenados($produto, "DESC");

		if(count($todos_bugs) == 0){
			$retorno['encontrados'] = 0;
			$retorno['corrigidos'] = 0;
			return $retorno;
		}

		//die(var_dump($todos_bugs));
		$data_inicio = date("Y-m-d", strtotime($todos_bugs[0]->creation_ts));
		$data_fim = date("Y-m-d");

		if($data_fim < date("Y-m-d", strtotime($todos_bugs[count($todos_bugs)-1]->delta_ts)))
			$data_fim = date("Y-m-d", strtotime($todos_bugs[count($todos_bugs)-1]->delta_ts));

		//Como eh o primeiro e foi ordena do por ele, posso pegar com esse numero magico
		$data_atual = date("Y-m-d", strtotime($todos_bugs[0]->creation_ts));	
		$encontrados = array();
		$soma_encontrados = 0;
		while($data_atual <= $data_fim ){
			foreach ($todos_bugs as $item) {
				if( $data_atual == date("Y-m-d", strtotime($item->creation_ts) ) ){
					$soma_encontrados ++;
				}
			}
			$encontrados[$data_atual] = $soma_encontrados;
			$data_atual = date("Y-m-d", strtotime( date("Y-m-d", strtotime($data_atual) ). " + 1 day"));
		}
		$data_atual = date("Y-m-d", strtotime($todos_bugs[0]->creation_ts));	
		$corrigidos = array();
		$soma_corrigidos = 0;
		while($data_atual <= $data_fim ){
			foreach ($todos_bugs as $item) {
				if( $data_atual == date("Y-m-d", strtotime($item->delta_ts) ) AND ($item->is_open == 0) ){
					$soma_corrigidos ++;
				}
			}
			$corrigidos[$data_atual] = $soma_corrigidos;
			$data_atual = date("Y-m-d", strtotime( date("Y-m-d", strtotime($data_atual) ). " + 1 day"));
		}


		//die(var_dump($abertos));
		$retorno['encontrados'] = $encontrados;
		$retorno['corrigidos'] = $corrigidos;
		return $retorno;
		
	}

	public function _graficoReabertos($produto = 1){


		$todas_ocorrencias_reabertos = $this->bug->todasOcorrenciasDefeitosReabertos($produto, 'ASC');

		if(count($todas_ocorrencias_reabertos) == 0){
			$retorno['reabertos'] = 0;
			return $retorno;
		}

		//die(var_dump($todas_ocorrencias_reabertos));
		$data_inicio = date("Y-m-d", strtotime($todas_ocorrencias_reabertos[0]->creation_ts));
		$data_fim = date("Y-m-d");
		//Como eh o primeiro e foi ordena do por ele, posso pegar com esse numero magico
		$data_atual = date("Y-m-d", strtotime($todas_ocorrencias_reabertos[0]->creation_ts));	
		$reabertos = array();
		$soma_reabertos = 0;
		while($data_atual <= $data_fim ){
			$soma_reabertos = 0;
			foreach ($todas_ocorrencias_reabertos as $item) {
				if( $data_atual == date("Y-m-d", strtotime($item->bug_when) ) ){
					$soma_reabertos ++;
				}
			}
			$reabertos[$data_atual] = $soma_reabertos;
			$data_atual = date("Y-m-d", strtotime( date("Y-m-d", strtotime($data_atual) ). " + 1 day"));
		}


		$retorno['reabertos'] = $reabertos;


		return $retorno;
	}

	public function _graficoTestesExecutados($produto = 1){

		$buildsProduto = $this->bug->buildsProduto($produto, 'DESC');
		$testCaseRunsBuild = $this->bug->testCaseRunsPorBuild($buildsProduto[0]->build_id);

		$totalCasosBuildRun = count($testCaseRunsBuild);
		
		$totalExecutados = 0;
		$totalNaoExecutados = 0;

		foreach ($testCaseRunsBuild as $item) {
			if($item->case_run_status_id == 2 OR $item->case_run_status_id == 3)
				$totalExecutados++;
		}

		$totalNaoExecutados = $totalCasosBuildRun - $totalExecutados;

		$array_final = array();

		$array_final['Executados'] = $totalExecutados;
		$array_final['Não Executados'] = $totalNaoExecutados;

		return $array_final;  

		//die(var_dump($totalNaoExecutados));

	}

	public function _graficoTestesAprovados($produto = 1){

		$buildsProduto = $this->bug->buildsProduto($produto, 'DESC');
		$testCaseRunsBuild = $this->bug->testCaseRunsPorBuild($buildsProduto[0]->build_id);

		$totalCasosBuildRun = count($testCaseRunsBuild);
		
		$totalAprovados = 0;
		$totalNaoExecutados = 0;

		foreach ($testCaseRunsBuild as $item) {
			if($item->case_run_status_id == 2)
				$totalAprovados++;
		}

		$totalNaoExecutados = $totalCasosBuildRun - $totalAprovados;

		$array_final = array();

		$array_final['Executados e aprovados'] = $totalAprovados;
		$array_final['Não Executados ou reprovados'] = $totalNaoExecutados;

		return $array_final;  
		//die(var_dump($totalNaoExecutados));
	}

	public function _graficoTestesAprovadosCategoria($produto = 1, $categoria){

		$buildsProduto = $this->bug->buildsProduto($produto, 'DESC');
		$testCaseRunsBuild = $this->bug->testCaseRunsPorBuild($buildsProduto[0]->build_id, $categoria);

		$categorias = $this->bug->categoriasTestCaseProduto($produto);

		$totalCasosBuildRun = count($testCaseRunsBuild);

		if($totalCasosBuildRun == 0){
			$array_final['Executados e aprovados'] = 0;
			$array_final['Não Executados ou reprovados'] = 0;
			$array_final['Categoria'] = 0;
			$array_final['listacategorias'] = $categorias;
			return $array_final;
		}
		
		$totalAprovados = 0;
		$totalNaoExecutados = 0;

		foreach ($testCaseRunsBuild as $item) {
			if($item->case_run_status_id == 2)
				$totalAprovados++;
		}

		$totalNaoExecutados = $totalCasosBuildRun - $totalAprovados;

		$array_final = array();



		$array_final['Executados e aprovados'] = $totalAprovados;
		$array_final['Não Executados ou reprovados'] = $totalNaoExecutados;
		$array_final['Categoria'] = $testCaseRunsBuild[0]->name;
		$array_final['listacategorias'] = $categorias;

		return $array_final;  
		//die(var_dump($totalNaoExecutados));


	}

}


/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
