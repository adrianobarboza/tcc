<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Bug extends CI_Model {

	function __construct(){
		parent::__construct();
	}

	function todosBug($produto = 1){
		$this->db->where("bugs.product_id", $produto);
		return $this->db->get("bugs")->result();
	}


	function todasOcorrenciasDefeitosReabertos($produto = 1, $ordem){
		$this->db->select('bugs_activity.bug_when, bugs.creation_ts');
		$this->db->from('bugs, bugs_activity');
		$this->db->where("bugs_activity.bug_id = bugs.bug_id");
		$this->db->where("bugs.product_id", $produto);
		$this->db->where("bugs_activity.added", 'REOPENED');
		$this->db->order_by('bugs.creation_ts', $ordem);
		$result = $this->db->get()->result();
		return $result;
	}

	function defeitosOrdenados($produto = 1, $ordem){
		$this->db->select('bug_status.is_open, bugs.creation_ts, bugs.delta_ts');
		$this->db->from('bugs, bug_status');
		$this->db->where('bugs.bug_status = bug_status.value');
		$this->db->where("bugs.product_id", $produto);
		$this->db->order_by('creation_ts',$ordem);
		$result = $this->db->get()->result();
		//die($this->db->last_query());
		//die(var_dump($result));
		return $result;
	}

	function defeitosFechadosOrdenados($idProduto, $ordem){
		$this->db->select('bugs.creation_ts, bugs.delta_ts');
		$this->db->from('bugs, products, bug_status');
		$this->db->where('bugs.product_id = products.id');
		$this->db->where('bugs.bug_status = bug_status.value');
		$this->db->where('bug_status.is_open', 0);
		$this->db->where('bugs.product_id',$idProduto);
		$this->db->order_by('bugs.delta_ts',$ordem);
		$result = $this->db->get()->result();
		return $result;

	}


	function defeitosCriticalOrdenados($produto = 1, $ordem){
		$this->db->select('bug_status.is_open, bugs.creation_ts, bugs.delta_ts');
		$this->db->from('bugs, bug_status');
		$this->db->where('bugs.bug_status = bug_status.value');
		$this->db->where("bugs.product_id", $produto);
		$this->db->where('bugs.bug_severity','critical');
		$this->db->order_by('creation_ts',$ordem);
		$result = $this->db->get()->result();
		//die($this->db->last_query());
		return $result;
	}

	function defeitosCriticalFechadosOrdenados($produto = 1, $ordem){
		$this->db->select('bugs.creation_ts, bugs.delta_ts');
		$this->db->from('bugs, products, bug_status');
		$this->db->where('bugs.product_id = products.id');
		$this->db->where('bugs.bug_status = bug_status.value');
		$this->db->where('bug_status.is_open', 0);
		$this->db->where('bugs.product_id',$produto);
		$this->db->where('bugs.bug_severity','critical');
		$this->db->order_by('bugs.delta_ts',$ordem);
		$result = $this->db->get()->result();
		return $result;
	}

	function defeitosEncontradosPorTipo($produto = 1){
		$this->db->select('bugs.cf_bugtype');
		$this->db->from('bugs');
		$this->db->where('bugs.product_id',$produto);
		$result = $this->db->get()->result();
		return $result;
	}

	function defeitosTodosProdutosEncontradosPorTipo(){
		$this->db->select('bugs.cf_bugtype');
		$this->db->from('bugs');
		$result = $this->db->get()->result();
		return $result;
	}

	function todosProfiles(){
		return $this->db->get("profiles")->result();
	}

	function todosBugsPorBuild($build){
		$this->db->from('bugs, test_case_bugs, test_case_runs');
		$this->db->select('*');
		$this->db->where('bugs.bug_id = test_case_bugs.bug_id');
		$this->db->where('test_case_bugs.case_run_id = test_case_runs.case_run_id');
		$this->db->where('test_case_runs.build_id',$build);
		$result = $this->db->get()->result();
		//die($this->db->last_query());
		return $result;
	}

	function defeitosAbertosPorSeveridade($idProduto){
		$this->db->select('bugs.bug_severity');
		$this->db->from('bugs, bug_status ');
		$this->db->where('bugs.bug_status = bug_status.value');
		$this->db->where('bug_status.is_open', 1);
		$this->db->where('bugs.product_id',$idProduto);
		$result = $this->db->get()->result();
		return $result;
	}

	function defeitosAbertosPorTipo($idProduto){
		$this->db->select('bugs.cf_bugtype');
		$this->db->from('bugs, bug_status');
		$this->db->where('bugs.bug_status = bug_status.value');
		$this->db->where('bug_status.is_open', 1);
		$this->db->where('bugs.product_id',$idProduto);
		$result = $this->db->get()->result();
		return $result;
	}

	function buildsProduto($produto, $ordem){
		$this->db->select('test_builds.build_id');
		$this->db->from('test_builds');
		$this->db->where('test_builds.product_id', $produto);
		$this->db->order_by('test_builds.build_id',$ordem);
		$result = $this->db->get()->result();
		return $result;
	}

	function testCaseRunsPorBuild($build, $categoria = 0, $ordem = 'ASC'){
		$this->db->select('test_case_runs.case_run_status_id, test_case_categories.name, running_date, close_date');
		$this->db->from('test_case_runs, test_case_categories, test_cases');
		$this->db->where('test_case_runs.build_id', $build);
		$this->db->where('test_cases.case_id = test_case_runs.case_id');
		$this->db->where('test_cases.category_id = test_case_categories.category_id');
		if($categoria != 0)
			$this->db->where('test_cases.category_id', $categoria);
		$this->db->order_by('test_case_runs.close_date',$ordem);
		$result = $this->db->get()->result();
		//die($this->db->last_query());
		//die(var_dump($result));
		return $result;
	}

	function categoriasTestCaseProduto($produto = 1) {
		$this->db->select('test_case_categories.name, test_case_categories.category_id, ');
		$this->db->from('test_case_categories');
		$this->db->where('test_case_categories.product_id', $produto);
		$result = $this->db->get()->result();
		return $result;
	}

}