<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Produto extends CI_Model {

	function __construct(){
		parent::__construct();
	}

	function todosProdutos(){
		return $this->db->get("products")->result();
	}

}