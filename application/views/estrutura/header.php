<html>
	<head>

		<meta charset="utf-8"> 
		
		<!-- Jquery -->
		<!-- script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script-->
		<script src="<?php echo base_url('public_html/js/jquery.min.js')?>"></script>

		<!-- jqPlot Charts -->
		<script language="javascript" type="text/javascript" src="<?php echo base_url('public_html/js/chart/jquery.jqplot.min.js')?>"></script>
		<link rel="stylesheet" type="text/css" href="<?php echo base_url('public_html/css/chart/jquery.jqplot.css')?>" />

		<!-- Twitter Bootstrap-->
		<link rel="stylesheet" href="<?php echo base_url('public_html/css/chart/bootstrap.min.css')?>">
		<link rel="stylesheet" href="<?php echo base_url('public_html/css/chart/bootstrap-theme.min.css')?>">
		<script src="<?php echo base_url('public_html/js/bootstrap.min.js')?>"></script>

	</head>

	<body>
		<div class="container">

			<div class="navbar navbar-default" role="navigation">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<a class="navbar-brand" href="<?php echo site_url('') ?>"">Painel de Acompanhamento</a>
				</div>
				<div class="navbar-collapse collapse">
					<ul class="nav navbar-nav">
						<!--li class="active"><a href="#">Link</a></li>
						<li><a href="#">Link</a></li>
						<li><a href="#">Link</a></li-->
						<li class="dropdown">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown">Gráficos e Relatórios <b class="caret"></b></a>
							<ul class="dropdown-menu">
								<li class="dropdown-header">Por todos produtos</li>
								<li><a href="<?php echo site_url('indicador/grafico_defeitos_todos_produtos_encontrado_tipo')?>" > Tipos de defeitos mais encontrados </a></li>
								<!--li><a href="#">Another action</a></li>
								<li><a href="#">Something else here</a></li-->
								<li class="divider"></li>
								<li class="dropdown-header">Por produto específico</li>
								<li><a href="<?php echo site_url('indicador/grafico_aberto_severidade')?>" > Defeitos abertos por severidade </a></li>
								<li><a href="<?php echo site_url('indicador/grafico_aberto_tipo')?>" > Defeitos abertos por tipo </a></li>
								<li><a href="<?php echo site_url('indicador/grafico_encontrados_corrigidos')?>" > Encontrados VS Corrigidos </a></li>
								<li><a href="<?php echo site_url('indicador/grafico_encontrados_corrigidos_critical')?>" > Encontrados VS Corrigidos Graves</a></li>
								<li><a href="<?php echo site_url('indicador/grafico_reabertos')?>" > Defeitos Reabertos</a></li>
								<li><a href="<?php echo site_url('indicador/grafico_encontrado_tipo')?>" > Tipos de defeitos mais encontrados </a></li>
								<li><a href="<?php echo site_url('indicador/grafico_testes_executados')?>" > Cobertura de Testes</a></li>
								<li><a href="<?php echo site_url('indicador/grafico_testes_aprovados')?>" > Cobertura de Testes Aprovados</a></li>
								<li><a href="<?php echo site_url('indicador/grafico_testes_aprovados_categoria/2')?>" > Cobertura de Testes Aprovados por Categoria</a></li>
								<li><a href="<?php echo site_url('indicador/grafico_testes_aprovados_tempo')?>" > Cobertura de Testes Aprovados ao longo do tempo</a></li>

							</ul>
						</li>
					</ul>
					<!--ul class="nav navbar-nav navbar-right">
						<li class="active"><a href="./">Default</a></li>
						<li><a href="../navbar-static-top/">Static top</a></li>
						<li><a href="../navbar-fixed-top/">Fixed top</a></li>
					</ul-->
				</div><!--/.nav-collapse -->
			</div>


