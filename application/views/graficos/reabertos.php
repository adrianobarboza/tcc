
<?php //var_dump($abertos_por_severidade) ?>


<script type="text/javascript" src="<?php echo base_url('public_html/js/chart/jqplot.highlighter.min.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('public_html/js/chart/jqplot.cursor.min.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('public_html/js/chart/jqplot.dateAxisRenderer.min.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('public_html/js/chart/jqplot.pointLabels.min.js')?>"></script>


<script type="text/javascript">


	$(document).ready(function(){

	  	var line1=[
	  		<?php foreach ($reabertos as $chave => $item) {
	  		echo "['".$chave."', ".$item."],";
	  		}?>
	  	];
	  	

		var plot1 = $.jqplot('chartdiv', [line1], {
		  title:'Defeitos Reabertos',
		  animate: true,
		  <!--animateReplot: true,-->
		  axes:{
		    xaxis:{
		      renderer:$.jqplot.DateAxisRenderer,

		      tickOptions:{
		        formatString:'%b&nbsp;%#d'
		      } 
		    },
		    yaxis:{
		      tickOptions:{
		        formatString:'%.0f'
		        }
		    }
		  },
		  series: [
		  	 {color: 'orange',
		  		//label: 'reabertos',
		  		//fill: true
		  	}
		  ],
		  highlighter: {
		    show: true,
		    sizeAdjust: 7.5
		  },
		  cursor: {
		    show: true,
		    zoom: true,
		    showTooltip: true
		  },
		});
	});
	
</script>	

<div class="row">
	<div class="col-md-2">
		<div class="btn-group">
			<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
				Produtos
			<span class="caret"></span>
			</button>
			<ul class="dropdown-menu">
				<?php foreach($todos_produtos as $value){ ?>
					<li><a href="<?php echo site_url('indicador/grafico_reabertos/'. $value->id)?>"><?php echo $value->name?></a></li>
				<?php }?>
			</ul>
		</div>
	</div>
	<div class="col-md-10">
		<div class="jumbotron">
			<div id="chartdiv" ></div>								
		</div>

	</div>
</div>




