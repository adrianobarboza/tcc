
<?php //var_dump($testes) ?>

<script language="javascript" type="text/javascript" src="<?php echo base_url('public_html/js/chart/jqplot.pieRenderer.min.js')?>"></script>

<script type="text/javascript">

	$(document).ready(function(){
	  var data = [
	  	<?php foreach ($testes as $key => $value) {
	  		echo "['".$key."', ".$value."] ,";
	  	}?>
	  ];
	  var plot1 = jQuery.jqplot ('chartdiv', [data], 
	    { 

	      title:
	      	<?php 
	      		echo "'Cobertura de Testes de ".$categoria." Aprovados'";
	      	?>,
	      animate: !$.jqplot.use_excanvas,
	      seriesDefaults: {
	        // Make this a pie chart.
	        shadow: false,
	        renderer: jQuery.jqplot.PieRenderer, 
	        rendererOptions: {
	          // Put data labels on the pie slices.
	          // By default, labels show the percentage of the slice.
	          padding: 2,
	          sliceMargin: 2,
	          showDataLabels: true
	        }
	      }, 
	      legend: { show:true, location: 'e' },
	      seriesColors: [ "#4bb2c5", "#ff6666" ]
	    }
	  );
	});
	
</script>	

<div class="row">
	<div class="col-md-2">
		<div class="btn-group">
			<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
				Categorias
			<span class="caret"></span>
			</button>
			<ul class="dropdown-menu">
				<?php foreach($listacategorias as $value){ ?>
					<li><a href="<?php echo site_url('indicador/grafico_testes_aprovados_categoria/'. $value->category_id)?>"><?php echo $value->name?></a></li>
				<?php }?>
			</ul>
		</div>
	</div>
	<div class="col-md-10">
		<div class="jumbotron">
			<!--<h2>Defeitos abertos por severidade</h2>!-->
			<div id="chartdiv" ></div>								
		</div>

	</div>
</div>




