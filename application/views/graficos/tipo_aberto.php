
<?php //var_dump($abertos_por_tipo) ?>

<script language="javascript" type="text/javascript" src="<?php echo base_url('public_html/js/chart/jqplot.pieRenderer.min.js')?>"></script>

<script type="text/javascript" src="<?php echo base_url('public_html/js/chart/jqplot.barRenderer.min.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('public_html/js/chart/jqplot.pieRenderer.min.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('public_html/js/chart/jqplot.categoryAxisRenderer.min.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('public_html/js/chart/jqplot.pointLabels.min.js')?>"></script>



<script type="text/javascript">

	$(document).ready(function(){
        $.jqplot.config.enablePlugins = true;

       	var data1 = [
		  	<?php foreach ($abertos_por_tipo as $key => $value) {
		  		echo "".$value." ,";
		  	}?>
	    ];

	    var ticks = [
		  	<?php foreach ($abertos_por_tipo as $key => $value) {
		  		echo "'".$key."', ";
		  	}?>
	    ];
         
        plot1 = $.jqplot('chartdiv', [data1], {
            // Only animate if we're not using excanvas (not in IE 7 or IE 8)..
            animate: !$.jqplot.use_excanvas,
            title:'Defeitos Abertos por Tipo',
            seriesDefaults:{
                renderer:$.jqplot.BarRenderer,
                pointLabels: { show: true }
            },
            axes: {
                xaxis: {
                    renderer: $.jqplot.CategoryAxisRenderer,
                    ticks: ticks
                }
            },
            highlighter: { show: false }
        });
     
        $('#chart1').bind('jqplotDataClick', 
            function (ev, seriesIndex, pointIndex, data) {
                $('#info1').html('series: '+seriesIndex+', point: '+pointIndex+', data: '+data);
            }
        );

    });

	
</script>	

<div class="row">
	<div class="col-md-2">
		<div class="btn-group">
			<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
				Produtos
			<span class="caret"></span>
			</button>
			<ul class="dropdown-menu">
				<?php foreach($todos_produtos as $value){ ?>
					<li><a href="<?php echo site_url('indicador/grafico_aberto_tipo/'. $value->id)?>"><?php echo $value->name?></a></li>
				<?php }?>
			</ul>
		</div>
	</div>
	<div class="col-md-10">
		<div class="jumbotron">
			<div id="chartdiv" ></div>								
		</div>

	</div>
</div>




