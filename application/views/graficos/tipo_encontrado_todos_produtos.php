
<?php //var_dump($abertos_por_severidade) ?>

<script language="javascript" type="text/javascript" src="<?php echo base_url('public_html/js/chart/jqplot.pieRenderer.min.js')?>"></script>

<script type="text/javascript">

	$(document).ready(function(){
	  var data = [
	  	<?php foreach ($encontrados_por_tipo as $key => $value) {
	  		echo "['".$key."', ".$value."] ,";
	  	}?>
	  ];
	  var plot1 = jQuery.jqplot ('chartdiv', [data], 
	    { 

	      title: 'Tipos de defeitos mais encontrados em todos os produtos',
	      animate: !$.jqplot.use_excanvas,
	      seriesDefaults: {	
	        // Make this a pie chart.
	        shadow: false,
	        renderer: jQuery.jqplot.PieRenderer, 
	        rendererOptions: {
	          // Put data labels on the pie slices.
	          // By default, labels show the percentage of the slice.
	          padding: 2,
	          sliceMargin: 2,
	          showDataLabels: true
	        }
	      }, 
	      legend: { show:true, location: 'e' }
	    }
	  );
	});
	
</script>	

<div class="row">
	
	<div class="col-md-12">
		<div class="jumbotron">
			<div id="chartdiv" ></div>								
		</div>

	</div>
</div>




